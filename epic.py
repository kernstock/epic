#! /usr/bin/env python

from datetime import datetime
import hashlib
from pathlib import Path
import shutil

import click
import exifread

FILE_EXTENSIONS = ['.jpg', '.jpeg']
SUCCESS_SYMBOL = click.style(u'\u2714', fg='green')
WARN_SYMBOL = click.style(u'\u26A0', fg='yellow')
FAIL_SYMBOL = click.style(u'\u2718', fg='red')


@click.command(short_help='Manage pictures based on EXIF metadata.')
@click.option('-s', '--source',
              help='The path to look for picture files (defaults to the current working directory)',
              default=str(Path.cwd()), type=click.Path(exists=True))
@click.option('-d', '--destination',
              help='The path to copy picture files (defaults to the current working directory)',
              default=str(Path.cwd()), type=click.Path(exists=True))
@click.option('--hash/--no-hash', 'hash_',
              help='Suffix the file name with its md5 hash, if the destination file already exists',
              default=True, show_default=True)
@click.option('-r', '--rename',
              help='Rename the copied files with the date taken',
              is_flag=True)
@click.option('-P', '--rename-pattern',
              help='Date pattern (1989 C standard) for the new file name (if not available, microseconds are omitted)',
              default='%y-%m-%d %H-%M-%S %f', show_default=True)
@click.option('-p', '--prefix',
              help='Prefix for the new file name (e.g. "Photo ")',
              default='')
# TODO: Add recursive flag
# @click.option('-R', '--recursive',
#               help='Look for files in sub-directories of the source path',
#               is_flag=True, show_default=True)
@click.option('--dry-run',
              help="Don't copy or create anything (use verbose mode to see what's happening)",
              is_flag=True)
@click.option('-V', '--verbose',
              help="Verbose mode",
              is_flag=True)
def epic(source: str, destination: str, hash_: bool, rename: bool, rename_pattern: str,
         prefix: str, dry_run: bool, verbose: bool):
    source = Path(source)
    destination = Path(destination)
    # Filter valid files
    file_paths = []
    for file_path in source.iterdir():
        if file_path.is_file() and file_path.suffix.lower() in FILE_EXTENSIONS:
            file_paths.append(file_path)

    # Process files
    error_counter = 0
    warn_counter = 0
    for file_path in file_paths:
        with file_path.open('rb') as file:
            # Get EXIF data
            try:
                tags = exifread.process_file(file, details=False)
                date_time = datetime.strptime(str(tags['EXIF DateTimeOriginal']), '%Y:%m:%d %H:%M:%S')
                date_time_ms = tags.get('EXIF SubSecTimeOriginal')
                if date_time_ms:
                    date_time = date_time.replace(microsecond=date_time_ms)
            except KeyError:
                err_msg = click.style(f'Could not read EXIF date from file {file_path}.', fg='red')
                click.echo(err_msg, err=True)
                error_counter += 1
                continue

        # Copy files
        dest_dir_path = destination / str(date_time.year) / date_time.strftime('%m')
        if not dest_dir_path.exists():
            if verbose:
                click.echo(f'Creating missing directory {dest_dir_path}.')
            if not dry_run:
                dest_dir_path.mkdir(parents=True)
        if rename:
            if date_time_ms:
                stem = date_time.strftime(rename_pattern)
            else:
                stem = date_time.strftime(rename_pattern[:-3])
        else:
            stem = file_path.stem
        file_name = prefix + stem + file_path.suffix
        dest_file_path = dest_dir_path / file_name
        if dest_file_path.exists():
            if hash_:
                with file_path.open('rb') as file:
                    file_hash = hashlib.sha1(file.read()).hexdigest()
                file_name = dest_file_path.stem + '_' + file_hash + dest_file_path.suffix
                warn_msg = click.style(
                    f'Warning: File {dest_file_path} does already exist.'
                    f' Adding checksum to file name ({file_name}).', fg='yellow')
                click.echo(warn_msg)
                warn_counter += 1
                dest_file_path = dest_dir_path / file_name
            else:
                err_msg = click.style(f'Could not copy file {file_path}', fg='red')
                click.echo(err_msg + f': Destination file already exists ({dest_file_path}).', err=True)
                error_counter += 1
                continue
        if verbose:
            if dry_run:
                dry_run_flag = ' (dry-run)'
            else:
                dry_run_flag = ''
            click.echo(f"Copying {file_path} as '{file_name}' to {dest_dir_path}{dry_run_flag}.")
        if not dry_run:
            shutil.copy2(file_path, dest_file_path)

    processed = len(file_paths)
    succeeded = processed - error_counter
    click.echo(f'Processed {processed} files. ' + SUCCESS_SYMBOL + f' {succeeded} succeeded, '
               + WARN_SYMBOL + f' {warn_counter} warnings, '
               + FAIL_SYMBOL + f' {error_counter} failed.')


epic()
